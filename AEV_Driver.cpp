/*
Joseph Vaiz-Gomez
Main Driver File of AEV Program
*/
#include <iostream>
#include <string>
#include <vector>
#include "AEV.h"

using namespace std;

int main() {
	vector<AEV> Vehicles;

	string temp_name, temp_model, temp_rank; // Temporary values for user to enter Name, Model, and Rank
	int temp_hp; // Temporary value for user to enter Horsepower
	double temp_ef; // Temporary value for user to enter Efficiency
	bool temp_oper; // Temporary value for user to enter Operability

	int selectedAEV; // Value to store position of desired AEV to be viewed

	int choice;
	int choice_3;
	int choice_3_1_1;
	
	char choice_0;
	char choice_3_1;

	bool menuOn = true;
	bool menu3On = true;
	bool exit_3_1_1;

	bool validAnswer;
	bool choosingAEV;
	bool firstInitialization = true;
	while (menuOn != false){
		if(firstInitialization == true)
		{
			cout << "Greetings, \nThank you for choosing Vatic Gen Technologies TM\n"
			     << "\nALL\nENVIRONMENT\nVEHICLE\nUser Interface\n\nDatabase Initialized: YES\n"
			     << "Clearance Granted: YES\n\n" 
			     << "Access granted to: GLOBAL AEV DATABASE\nKeyphrase: PROJECT EXODUS"
			     << "\nRegistered Party: UNITED NATIONS\n"
			     << "Clearance Granted: LEVEL 4\n\n";
			firstInitialization = false;
		}
		cout << "o-------------------------------------------o\n";
		cout << "o 1 - Log a new AEV                         o\n";
		cout << "o 2 - Get Individualized AEV Info           o\n";
		cout << "o 3 - Delete or Resubmit AEV Spec Info      o\n";
		cout << "o 4 - Display All Logged AEV Info           o\n";
		cout << "o 5 - Logout                                o\n";
		cout << "o-------------------------------------------o\n";
		cout << " Enter your choice and press return: ";

		cin >> choice;

		cout << endl;

		switch (choice)
		{
		case 1:
			cout << "Prepare to enter AEV specifications...\n\n";
			validAnswer = false;
			// rest of code here
			while(validAnswer == false)
			{
				cout << "Enter Name or Make of AEV: ";
				cin >> temp_name;
				cout << "Enter Model: ";
				cin >> temp_model;
				cout << "Enter Horsepower: ";
				cin >> temp_hp;
				cout << "Enter Efficiency (rating between 1.000 and 5.000): ";
				cin >> temp_ef;
				cout << "Enter Rank: ";
				cin >> temp_rank;
				cout << "Enter Operability of AEV, please enter a 1 or 0 (1 = Operable, 0 = Inoperable): ";
				cin >> temp_oper;
				if((temp_ef>=1.000 && temp_ef<=5.000) && (temp_oper==true || temp_oper==false))
				{
					validAnswer = true;
					Vehicles.push_back(AEV(temp_name, temp_model, temp_hp, temp_ef, temp_rank, temp_oper));
					cout << "\n";
				}
				else
				{
					cout << "You have entered one or more invalid specifications.\nRestarting submission process...\n\n";
				}

			}
			break;
		case 2:
			if (Vehicles.size() < 1)
			{
				cout << "No AEVs have been logged into the database at this point...\n\n";
			}
			else
			{
				choosingAEV = true;
				while (choosingAEV == true)
				{
					cout << "You may select from the list of AEVs logged as of now...\n\n";
					for (int i = 0; i < Vehicles.size(); i++)
						cout << "\t" << i + 1 << ". " << Vehicles[i].getName() << " " << Vehicles[i].getModel() << "\n";
					cout << "\nPlease enter space/slot of AEV: ";
					cin >> selectedAEV;
					if ((selectedAEV - 1) >= 0 && (selectedAEV - 1) < Vehicles.size())
					{
						cout << "\n\nHere are the specifications for the selected vehicle:\n\n";
						cout << "Technical Report:\n";
						Vehicles[selectedAEV - 1].displayInfo();
					}
					else
					{
						cout << "\nInvalid choice entered\n\n";
					}
					cout << "Would you like to view the info for another AEV?(Y/N): ";
					cin >> choice_0;
					if (choice_0 == 'N' || choice_0 == 'n')
					{
						choosingAEV = false;
					}
					cout << endl << endl;
				}
			}
			// rest of code here
			break;
		case 3:
			if (Vehicles.size() < 1) 
			{
				cout << "No AEVs have been logged into the database at this point...\n\n";
			}
			else
			{
				menu3On = true;
				while (menu3On != false)
				{
					cout << "Prepare to make changes to the AEV Database...\n\n";
					cout << "o-------------------------------------------o\n";
					cout << "o 1 - Remove an AEV from the Database       o\n";
					cout << "o 2 - Modify specifications for an AEV      o\n";
					cout << "o 3 - Exit to Main Menu                     o\n";
					cout << "o-------------------------------------------o\n";
					cout << " Enter your choice and press return: ";

					cin >> choice_3;

					cout << endl;

					switch (choice_3)
					{
					case 1:
						//This will be fairly simple
						//This is basically the same code from selecting the specifications report from the AEV Database.
						choosingAEV = true;
						while (choosingAEV == true)
						{
							cout << "Prepare to select an AEV to be removed from the Database...\n\n";
							for (int i = 0; i < Vehicles.size(); i++)
								cout << "\t" << i + 1 << ". " << Vehicles[i].getName() << " " << Vehicles[i].getModel() << "\n";
							cout << "\nPlease enter space/slot of AEV: ";
							cin >> selectedAEV;
							if ((selectedAEV - 1) >= 0 && (selectedAEV - 1) < Vehicles.size())
							{
								cout << "\n\nThe following AEV has been selected for deletion: ";
								cout << Vehicles[selectedAEV-1].getName() << " " << Vehicles[selectedAEV-1].getModel() << "\n";
								cout << "Are you sure you would like to remove the above AEV from the Database?(Y/N): ";
								cin >> choice_3_1;
								if (choice_3_1 == 'Y' || choice_3_1 == 'y')
								{
									Vehicles.erase(Vehicles.begin() + selectedAEV - 1);//Instead of (Vehicles.begin() + selectedAEV)
									cout << "The AEV has been deleted from the Database\n\n";
								}
								else
								{
									cout << "The AEV has NOT been deleted.\n\n";
								}
							}
							else
							{
								cout << "\nInvalid choice entered\n\n";
							}
							cout << "Would you like to remove another AEV from the Database?(Y/N): ";
							cin >> choice_0;
							if (choice_0 == 'N' || choice_0 == 'n')
							{
								choosingAEV = false;
							}
							cout << endl << endl;
						}
						break;
					case 2:
						//I'm probably going to have to make another while loop along with switch case to set configuration choices
						choosingAEV = true;
						while (choosingAEV == true)
						{
							cout << "Prepare to select the AEV to which modifications will be made... \n\n";
							for (int i = 0; i < Vehicles.size(); i++)
								cout << "\t" << i + 1 << ". " << Vehicles[i].getName() << " " << Vehicles[i].getModel() << "\n";
							cout << "\nPlease enter space/slot of AEV: ";
							cin >> selectedAEV;
							if ((selectedAEV - 1) >= 0 && (selectedAEV - 1) < Vehicles.size())
							{
								cout << "\n\nThe following AEV has been selected for modification: ";
								cout << Vehicles[selectedAEV - 1].getName() << " " << Vehicles[selectedAEV - 1].getModel() << "\n";
								cout << "Please select from the list of properties you would like to modify...\n";
								/*
								These are the changeable properties:
									1. [string] name;
									2. [string] model;
									3. [int] horsepower;
									4. [double] efficiency;
									5. [string] rank;
									6. [bool] operable;
								*/
								cout << "o-------------------------------------------o\n";
								cout << "o 1 - Name/Make                             o\n";
								cout << "o 2 - Model                                 o\n";
								cout << "o 3 - Horsepower                            o\n";
								cout << "o 4 - Efficiency                            o\n";
								cout << "o 5 - Rank                                  o\n";
								cout << "o 6 - Operability                           o\n";
								cout << "o 7 - Exit to General Database change menu  o\n";
								cout << "o-------------------------------------------o\n";
								cout << " Enter your choice and press return: ";
								cin >> choice_3_1_1;
								cout << endl;
								/*
								Available Temporary Values: 
								Name: [string] temp_name;
								Model: [string] temp_model;
								Horsepower: [int] temp_hp;
								Efficiency: [double] temp_ef;	NOTE: Should be a value within the range 1.000 to 5.000
								Rank: [string] temp_rank;
								Operability: [bool] temp_oper;	NOTE: 1 = Operable, 0 = Inoperable
								*/
								exit_3_1_1 = false; //I think this will fix the bug.
								switch (choice_3_1_1)
								{
								case 1:
									cout << "Please enter a new Name/Make for this AEV: ";
									cin >> temp_name;
									Vehicles[selectedAEV - 1].setName(temp_name);
									cout << "The AEV Name/Make has been changed.\n\n";
									break;
								case 2:
									cout << "Please enter a new Model for this AEV: ";
									cin >> temp_model;
									Vehicles[selectedAEV - 1].setModel(temp_model);
									cout << "The AEV Model has been changed.\n\n";
									break;
								case 3:
									cout << "Please enter a new Horsepower for this AEV: ";
									cin >> temp_hp;
									Vehicles[selectedAEV - 1].setHP(temp_hp);
									cout << "The AEV Horsepower has been changed.\n\n";
									break;
								case 4:
									cout << "Please enter a new Efficiency for this AEV (rating between 1.000 and 5.000): ";
									cin >> temp_ef;
									Vehicles[selectedAEV - 1].setEff(temp_ef);
									cout << "The AEV Efficiency has been changed.\n\n";
									break;
								case 5:
									cout << "Please enter a new Rank for this AEV: ";
									cin >> temp_rank;
									Vehicles[selectedAEV - 1].setRank(temp_rank);
									cout << "The AEV Rank has been changed.\n\n";
									break;
								case 6:
									cout << "Please enter a new Operability for this AEV (1 = Operable, 0 = Inoperable): ";
									cin >> temp_oper;
									Vehicles[selectedAEV - 1].setOp(temp_oper);
									cout << "The AEV Operability has been changed.\n\n";
									break;
								case 7:
									choosingAEV = false;
									exit_3_1_1 = true;
									cout << "Exiting to General Database change menu...\n\n";
									break;
								default:
									cout << "Not a valid choice, please choose again: \n";
									cout << "o-------------------------------------------o\n";
									cout << "o 1 - Name/Make                             o\n";
									cout << "o 2 - Model                                 o\n";
									cout << "o 3 - Horsepower                            o\n";
									cout << "o 4 - Efficiency                            o\n";
									cout << "o 5 - Rank                                  o\n";
									cout << "o 6 - Operability                           o\n";
									cout << "o 7 - Exit to General Database change menu  o\n";
									cout << "o-------------------------------------------o\n";
									cout << " Enter your choice and press return: ";
									cin >> choice_3_1_1;
									cout << endl;
									break;
								}
							}
							else
							{
								cout << "\nInvalid choice entered\n\n";
							}
							if (exit_3_1_1 != true)
							{
								cout << "Would you like to make any further modifications to the Database?(Y/N): ";
								cin >> choice_0;
								if (choice_0 == 'N' || choice_0 == 'n')
								{
									choosingAEV = false;
								}
								cout << endl << endl;
							}
						}
						break;
					case 3:
						menu3On = false;
						cout << "Exiting to Main Menu...\n\n";
						break;
					default:
						cout << "Not a valid choice, please choose again: \n";
						cout << "o-------------------------------------------o\n";
						cout << "o 1 - Remove an AEV from the Database       o\n";
						cout << "o 2 - Modify specifications for an AEV      o\n";
						cout << "o 3 - Exit to Main Menu                     o\n";
						cout << "o-------------------------------------------o\n";
						cout << " Enter your choice and press return: ";
						cin >> choice_3;
						cout << endl;
						break;
					}
				}
			}
			// rest of code here
			break;
		case 4:
			if (Vehicles.size() < 1)
			{
				cout << "No AEVs have been logged into the database at this point...\n\n";
			}
			else
			{
				cout << "The following report contains comprehensive AEV info from the local Database on this machine.\n";
				cout << "LOCAL AEV DATABASE REPORT:\n\n";
				for (int z = 0; z < Vehicles.size(); z++)
				{
					cout << "Technical Report " << z + 1 << ":\n";
					Vehicles[z].displayInfo();
				}
				cout << "END OF REPORT\n\n"
					<< "Returning to Main Menu...\n\n\n";
			}
			break;
		case 5:
			cout << "Signing out. Thank you for choosing Vatic Gen Technologies.\n";
			menuOn = false;
			break;
		default:
			cout << "Not a valid choice, please choose again: \n";
			cout << "o-------------------------------------------o\n";
			cout << "o 1 - Log a new AEV                         o\n";
			cout << "o 2 - Get Individualized AEV Info           o\n";
			cout << "o 3 - Resubmit AEV Spec Info                o\n";
			cout << "o 4 - Display All Logged AEV Info           o\n";
			cout << "o 5 - Logout                                o\n";
			cout << "o-------------------------------------------o\n";
			cout << " Enter your choice and press return: ";
			cin >> choice;
			break;
		}
	}
	return 0;
}