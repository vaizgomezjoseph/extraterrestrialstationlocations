/**********************************************************************************************************/
// My new AEV.cpp file
// Note that AEV stands for "All Environment Vehicles"

#include <iostream>
#include <string>
#include "AEV.h"
using namespace std; //spst01 shorthand for Space Station # 01

// Empty Constructor
AEV::AEV()
{
	string nm("Unspecified");
	string mo("Unspecified");
	int hp(0);
	double eff_score(0.0);
	string rnk("Unassigned");
	bool oper(false);
	name = nm;
	model = mo;
	horsepower = hp;
	efficiency = eff_score;
	rank = rnk;
	operable = oper;
}

AEV::AEV(string nm, string mo, int hp, double eff_score, string rnk, bool oper)
{
	name = nm;
	model = mo;
	horsepower = hp;
	efficiency = eff_score;
	rank = rnk;
	operable = oper;
}

AEV::~AEV()
{
}

string AEV::getName() const
{
	return name;
}

string AEV::getModel() const
{
	return model;
}

int AEV::getHP() const
{
	return horsepower;
}

double AEV::getEff() const
{
	return efficiency;
}

string AEV::getRank() const
{
	return rank;
}

bool AEV::getOp() const
{
	return operable;
}

void AEV::setName(string nm)
{
	name = nm;
}

void AEV::setModel(string mo)
{
	model = mo;
}

void AEV::setHP(int hp)
{
	horsepower = hp;
}

void AEV::setEff(double eff)
{
	efficiency = eff;
}

void AEV::setRank(string rnk)
{
	rank = rnk;
}

void AEV::setOp(bool oper)
{
	operable = oper;
}

void AEV::displayInfo()
{
	cout << "\tName and Model: " << name << " " << model
	     << "\n\tHorsepower: " << horsepower
	     << "\n\tEfficiency Score: " << efficiency
	     << "\n\tRank: " << rank
	     << "\n\tOperability: ";
	if(operable == true)
	{
		cout << "OPTIMUM\n\n" << endl;
	}
	else
	{
		cout << "INADEQUATE\n\n" << endl;
	}
}