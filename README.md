# README #

Welcome to my first repository! This repository was created upon following the basic BitBucket tutorial for
learning GIT. After completing the basics, I decided to get creative and implement a bit more functionality.

### What is this repository for? ###

* Quick summary
This repository functions as the primary database for a multi-billion dollar project to develop and supply
the software and documents to support a network of space stations and facilities that will propogate 
over Earth and throughout the solar system. It's also not real.

Many of the .txt files contained here consist of imaginary instructions, designations, designs, etc. for my
strictly theoretical plans to establish a galactic empire.
* Version
Current: 0.4
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)


### How do I get set up? ###

* Summary of set up
Not functional at this point.
* Configuration
* Dependencies
User should use Microsoft Visual Studio to build, compile, and run the program.
* Database configuration
* How to run tests
Tests will eventually be run using the open source Catch software.
* Deployment instructions
### Contribution guidelines ###

* Writing tests
Tests will eventually be able to be written in C++ files.
* Code review
* Other guidelines


### Who do I talk to? ###

* Repo owner or admin
Joseph Vaiz-Gomez
* Other community or team contributors