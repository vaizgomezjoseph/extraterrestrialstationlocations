/**********************************************************************************************************/
// My new AEV.h file
// Note that AEV stands for "All Environment Vehicles"

// Contributor(s): Joseph Vaiz-Gomez

#pragma once
#include <string>
using namespace std; //spst01 shorthand for Space Station # 01
#ifndef AEV_H
#define AEV_H

class AEV
{
public:
	AEV();
	AEV(string nm, string mo, int hp, double eff_score, string rnk, bool oper);
	~AEV();
	string getName() const;
	string getModel() const;
	int getHP() const;
	double getEff() const;
	string getRank() const;
	bool getOp() const;
	void setName(string nm);
	void setModel(string mo);
	void setHP(int hp);
	void setEff(double eff);
	void setRank(string rnk);
	void setOp(bool oper);
	void displayInfo();
protected:
private:
	string name;
	string model;
	int horsepower;
	double efficiency;
	string rank;
	bool operable;
};

#endif // !AEV_H